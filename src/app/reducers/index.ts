import { counterReducer } from './counter';

export interface AppState {
  counter: number;
}

export const rootReducer = {
  counter: counterReducer
};
