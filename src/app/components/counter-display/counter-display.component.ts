import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers/index';

@Component({
  selector: 'app-counter-display',
  template: `
    <div>Current Count: {{ counter | async }}</div>
  `
})
export class CounterDisplayComponent implements OnInit {

  counter: Observable<number>;

  constructor(private store: Store<AppState>) {
    this.counter = store.select('counter');
  }

  ngOnInit() {
  }

}
